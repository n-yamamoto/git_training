# 変更のコミット

Gitで変更をコミットする方法を学習します。

## この章で学ぶコマンド

- status
- diff
- log
- show
